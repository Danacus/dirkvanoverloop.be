// @flow

import 'babel-polyfill'
import ReactDOM from 'react-dom'
import React from 'react'
import App from './components/App/App'
import { Provider } from 'mobx-react'
import LanguageStore from './stores/LanguageStore'
import ImageStore from './stores/ImageStore'
import PageStore from './stores/PageStore'
import LocalizedStrings from 'react-localization'
import './index.css'
import locale from './locale.json'
import config from './config.json'

const root = document.getElementById('root')

const languageStore = new LanguageStore()
const pageStore = new PageStore()
const paintingStore = new ImageStore(config.imagesUrl, "schilderijen")
const drawingStore = new ImageStore(config.imagesUrl, "tekeningen")

languageStore.setLocalization(new LocalizedStrings(locale))

if (root) {
  ReactDOM.render(
    <Provider
      LanguageStore={languageStore}
      PaintingStore={paintingStore}
      DrawingStore={drawingStore}
      PageStore={pageStore}
    >
      <App />
    </Provider>,
    root
  )
}
