// @flow

import { observable, action } from 'mobx'
import XMLHttpRequestPromise from 'xhr-promise'

export default class ImageStore {
    @observable images: Array<string>
    @observable currentIndex: number;

    constructor(url: string, dir: string) {
        this.currentIndex = 0
        this.images = []
        this.loadImages(url, dir);

        document.addEventListener('keydown', (e: KeyboardEvent) => {
            if (e.key === 'ArrowRight') {
                this.next()
            }

            if (e.key === 'ArrowLeft') {
                this.prev()
            }
        })
    }

    async loadImages(url: string, dir: string) {
        let xhr = new XMLHttpRequestPromise()
        let files = await xhr.send({
            method: 'POST',
            url,
            data: 'dir=' + dir
        })
        let urls = JSON.parse(files.responseText).map(file => url + file)
        let sortedUrls = urls.sort((a, b) => {
            if (a < b) return -1
            else if (a > b) return 1
            return 0
        })
        this.setImages(sortedUrls)
    }

    @action
    setImages(images: Array<string>) {
        this.images = images
    }

    @action
    setIndex(index: number) {
        this.currentIndex = index
    }

    @action
    next() {
        if (this.currentIndex < this.images.length - 1) {
            this.currentIndex++
        } else {
            this.currentIndex = 0
        }
    }

    @action
    prev() {
        if (this.currentIndex === 0) {
            this.currentIndex = this.images.length - 1
        } else {
            this.currentIndex--
        }
    }
}
