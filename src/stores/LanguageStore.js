// @flow

import { observable, action, computed } from 'mobx'
import LocalizedStrings from 'react-localization'

export default class LanguageStore {
    @observable localizationFull: LocalizedStrings
    @observable localization: Object

    @computed get language() {
        return this.localization.getLanguage()
    }

    @action setLanguage(language: string) {
        this.localizationFull.setLanguage(language)
        this.localization = Object.assign({}, this.localizationFull)
        window.localStorage.setItem('lang', language)
    }

    @action setLocalization(localization: LocalizedStrings) {
        this.localizationFull = localization
        this.localization = Object.assign({}, this.localizationFull)
        if (window.localStorage.getItem('lang') !== null) {
            this.setLanguage(window.localStorage.getItem('lang'))
        }
    }
}
