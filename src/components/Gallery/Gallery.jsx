// @flow

import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import styles from './Gallery.css'
import ImageStore from '../../stores/ImageStore'
import { observable } from 'mobx'
import FontAwesome from 'react-fontawesome'

type Props = {
    ImageStore: ImageStore,
}

@observer
export default class Gallery extends Component<Props> {
    constructor(props: Props) {
        super(props)
    }

    getImageClass(index: number) {
        const current = this.props.ImageStore.currentIndex

        switch (index) {
            case current:
                return styles.current;
            case current - 1:
                return styles.prev;
            case current + 1:
                return styles.next;
            default:
                return '';
        }
    }

    render() {
        const { ImageStore } = this.props

        return (
            <div className={styles.root}>
                <div className={`${styles.gallery}`}>
                    {ImageStore.images.map((image, i) =>
                    <div key={i} className={`
                        ${styles.container}
                        ${this.getImageClass(i)}
                    `   }>
                        <img
                            src={image}
                            className={styles.image}
                        />
                    </div>
                    )}
                </div>

                <div onClick={() => ImageStore.prev()} className={`${styles.button} ${styles.left}`}>
                    <FontAwesome name='angle-left' className={`${styles.buttonIcon} ${styles.left}`} />
                </div>

                <div onClick={() => ImageStore.next()} className={`${styles.button} ${styles.right}`}>
                    <FontAwesome name='angle-right' className={`${styles.buttonIcon} ${styles.right}`} />
                </div>
            </div>
        )
    }
}
