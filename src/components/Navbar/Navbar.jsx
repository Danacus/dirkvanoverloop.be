// @flow

import React, { Component } from 'react'
import styles from './Navbar.css'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
import FontAwesome from 'react-fontawesome'

type Props = {
    LanguageStore: any,
    onClick: (string) => mixed
}

@inject('LanguageStore')
@observer
export default class Navbar extends Component<Props> {
    @observable
    opened: boolean

    @observable dropdownOpen: boolean

    constructor(props: Props) {
        super(props)
        this.opened = false
        this.dropdownOpen = false
    }

    render() {
        const strings = this.props.LanguageStore.localization

        return (
            <div>
                <div className={`${styles.menuButton} ${this.opened ? styles.open : ''}`} onClick={() => { this.opened = !this.opened }}><FontAwesome name='bars' className={styles.menuIcon} /></div>
                <ul onClick={(e) => this.props.onClick(e.target.id)} className={`${styles.list} ${this.opened ? styles.open : ''}`}>
                    <li className={`${styles.title}`}>Dirk Vanoverloop</li>
                    <li id='paintings'>{strings.paint}</li>
                    <li id='drawings'>{strings.draw}</li>
                    <li id='links'>{strings.links}</li>
                    <li id='contact'>{strings.contact}</li>
                    <li className={this.dropdownOpen ? styles.open : ''} onClick={() => { this.dropdownOpen = !this.dropdownOpen }}>
                        {strings.language}
                        <div className={`${styles.dropdown} ${this.dropdownOpen ? styles.open : ''}`}>
                            <p onClick={() => this.props.LanguageStore.setLanguage('en')}>{strings.en}</p>
                            <p onClick={() => this.props.LanguageStore.setLanguage('nl')}>{strings.nl}</p>
                        </div>
                    </li>
                </ul>
            </div>
        )
    }
}
