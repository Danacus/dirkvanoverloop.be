// @flow

import React from 'react'
import { inject, observer } from 'mobx-react'
import Navbar from '../Navbar/Navbar'
import Gallery from '../Gallery/Gallery'
import Links from '../Links/Links'
import Contact from '../Contact/Contact'
import ImageStore from '../../stores/ImageStore'
import LanguageStore from '../../stores/LanguageStore'
import PageStore from '../../stores/PageStore'
import scrollToComponent from 'react-scroll-to-component'
import styles from './App.css'

type Props = {
    LanguageStore: LanguageStore,
    PaintingStore: ImageStore,
    DrawingStore: ImageStore,
    PageStore: PageStore
}

@inject('LanguageStore', 'PaintingStore', 'DrawingStore', 'PageStore')
@observer
export default class App extends React.Component<Props> {
    constructor(props: Props) {
        super(props)
    }

    scroll = () => scrollToComponent(this.props.PageStore.currentPage, {offset: -10, ease: 'inOutCube'}) 
    componentDidUpdate = this.scroll;
    componentDidMount = this.scroll;

    render() {
        const { LanguageStore, PaintingStore, DrawingStore, PageStore } = this.props
        const strings = LanguageStore.localization

        // Dirty hack to force Mobx to update this component
        const currentPageId = this.props.PageStore.currentPageId

        return (
            <div>
                <Navbar onClick={(id) => { if (id) { 
                    this.props.PageStore.currentPageId = id 
                    this.scroll()
                }}} />
                <Gallery ref={(c) => { PageStore.pages.paintings = c }} ImageStore={PaintingStore} />
                <Gallery ref={(c) => { PageStore.pages.drawings = c }} ImageStore={DrawingStore} />
                <Links ref={(c) => { PageStore.pages.links = c }} />
                <Contact ref={(c) => { PageStore.pages.contact = c }} />
            </div>
        )
    }
}
