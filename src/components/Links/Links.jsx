// @flow

import React, { Component } from 'react'
import styles from './Links.css'

type Props = {}

export class Links extends Component<Props> {
  render() {
    return (
      <div className={styles.root}>
        <ul>
          <li><b>Links</b></li>
          <li><a href='http://www.ingeoeyen.be/'>Inge Oeyen</a></li>
          <li><a href='http://www.philippedesmedt.be/'>Philippe De Smedt</a></li>
          <li><a href='http://www.wardlernout.be/'>Ward Lernout</a></li>
          <li><a href='http://users.telenet.be/leopoldgeysen/'>Leopold Geysen</a></li>
        </ul>
      </div>
    )
  }
}

export default Links
