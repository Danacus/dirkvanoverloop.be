// @flow

import React, { Component } from 'react'
import styles from './Contact.css'
import LanguageStore from '../../stores/LanguageStore'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
import XMLHttpRequestPromise from 'xhr-promise'

type Props = {
  LanguageStore: LanguageStore
}

@inject('LanguageStore')
@observer
export class Contact extends Component<Props> {
  @observable name: string;
  @observable email: string;
  @observable message: string;

  constructor(props: Props) {
    super(props)

    this.name = ''
    this.email = ''
    this.message = ''
  }

  submit = async () => {
    let xhr = new XMLHttpRequestPromise()
    let res = await xhr.send({
      method: 'POST',
      url: 'https://dirkvanoverloop.be/contact.php',
      data: `name=${this.name}&email=${this.email}&message=${this.message}`
    })

    const strings = this.props.LanguageStore.localization

    console.log(res.responseText)

    if (res.responseText.trim() === 'ok') {
      window.alert(strings.ok)
    } else if (res.responseText.trim() === 'infoErr') {
      window.alert(strings.infoErr)
    } else {
      window.alert(strings.mailErr)
    }

    this.name = ''
    this.email = ''
    this.message = ''
  }

  render() {
    const strings = this.props.LanguageStore.localization

    return (
      <div className={styles.root}>
        <div>
          {strings.name}:<br />
          <input onChange={(e) => { this.name = e.target.value }} type='text' name='name' placeholder='' /><br />
          {strings.email}:<br />
          <input onChange={(e) => { this.email = e.target.value }} type='email' name='email' placeholder='' /><br />
          {strings.message}:<br />
          <textarea onChange={(e) => { this.message = e.target.value }} rows={10} type='text' name='message' placeholder='' /><br />
          <button onClick={() => this.submit()} name='submit'>{strings.submit}</button>
        </div>
      </div>
    )
  }
}

export default Contact
